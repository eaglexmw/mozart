/**
 * Mozart++ Template Library
 * Licensed under MIT License
 * Copyright (c) 2020 Covariant Institute
 * Website: https://covariant.cn/
 * Github:  https://github.com/covariant-institute/
 */

/**
 * dummy.cpp
 *
 * This is a dummy source file so it defines nothing and
 * implements nothing. But CLion needs at least one source file
 * to enable code insight and some related features.
 */
